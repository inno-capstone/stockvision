# Quick Start

## Requirements

- nvm (optional)
    ```bash
    nvm use
    nvm install # if you don't have a propper version
    ```
- node [with this version](../.nvmrc) if you don't have nvm
- docker
- docker-compose

## Start dev server locally

1. Create `/backend/.env` file and copy all stuff from `.env.example` to `.env`

2. Install dependecies
    ```bash
    npm install
    ```

3. Start servicies

   `migrate` service firing errors. It's ok, just do it manually

    ```bash
    docker-compose build && docker-compose up
    ```

5. Run backend
    ```bash
    npm run migrate up # for first time
    npm run dev:openapi
    ```
