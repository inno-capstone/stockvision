import {RouteHandler} from 'fastify'
import {RouteOptionsWithGeneric} from '../types'
import {FastifySchema} from 'fastify/types/schema'
import {generatePasswordHash} from '../../utils/password-hashing'

const OK = 200
const ERROR = 500
const USER_ALREADY_EXISTS = 201

type CreateUserRouteGeneric = {
  Body: {
    username: string
    password: string
  }
}

type CreateUserHandler = RouteHandler<CreateUserRouteGeneric>

const handler: CreateUserHandler = (request, reply) => {
  const {pg} = request

  if (!pg) {
    return reply.code(ERROR).send({'error': 'Do not have database registered'})
  }

  const {body} = request
  const {username, password} = body

  pg.query('select id from users where username=$1', [username],
    async (err, result) => {
      if (err) {
        return reply.code(ERROR).send({error: 'Cannot select: ' + err})
      }

      if (result.rowCount > 0) {
        return reply.code(USER_ALREADY_EXISTS).send()
      }

      const hash = await generatePasswordHash(password)

      pg.query('insert into users(username, password) values ($1, $2)', [username, hash],
        async (err, _result) => {
          if (err) {
            return reply.code(ERROR).send({error: 'Cannot select: ' + err})
          }
          const token = await reply.jwtSign({username})
          return reply.code(OK).send(token)
        }
      )
    }
  )
}

const schema: FastifySchema = {
  tags: ['database'],
  body: {
    type: 'object',
    required: ['username', 'password'],
    properties: {
      username: {type: 'string'},
      password: {type: 'string'}
    },
  },
  response: {
    [OK]: {
      type: 'string',
      description: 'User created, return JWT'
    },
    [USER_ALREADY_EXISTS]: {
      type: 'string',
      description: 'User already exists'
    },
    [ERROR]: {
      type: 'object',
      descriptions: 'Something went wrong',
      properties: {
        error: { type: 'string' }
      }
    }
  }
}

export const createUser: RouteOptionsWithGeneric<CreateUserRouteGeneric> = {
  method: 'POST',
  url: '/create-user',
  handler,
  schema,
}