import postgres from '@fastify/postgres'

import {Router} from '../types'
import {POSTGRES_CONFIG} from '../../configs'
import {createUser} from './create-user'
import {listUsers} from './list-users'
import {PoolClient} from 'pg'

export const router: Router = (fastify, opt, done) => {
  fastify.register(postgres, POSTGRES_CONFIG)

  fastify.addHook('preHandler', async (request, reply) => {
    try {
      request.pg = await fastify.pg.connect()
    } catch (e) {
      return reply.code(500).send({error: 'Cannot connect to pg' + e})
    }
  }).addHook('onResponse', (request, _reply) => {
    if (request.pg) {
      request.pg.release()
    }
  })

  fastify.route(createUser)
  fastify.route(listUsers)

  fastify.get('/user/1', async (req, reply) => {
    const client = req.pg

    if (!client) {
      fastify.log.error('TEST poolClient missing')
      return reply.code(500).send('TEST poolClient missing')
    }
    
    try {
      const result = await client.query('SELECT id, username FROM users WHERE id=$1', [1])
      reply.send(result)
    } catch (e) {
      return reply.code(500).send({error: 'Cannot select' + e})
    }
  })

  done()
}