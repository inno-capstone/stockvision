import {RouteHandler} from 'fastify'
import {RouteOptionsWithGeneric} from '../types'
import {FastifySchema} from 'fastify/types/schema'
import {generatePasswordHash} from '../../utils/password-hashing'

const OK = 200
const ERROR = 500
const USER_DOESNT_EXISTS = 201

type GetUserRouteGeneric = {
  Body: {
    username: string
  }
}

type GetUserHandler = RouteHandler<GetUserRouteGeneric>

const handler: GetUserHandler = (request, reply) => {
  const {pg} = request

  if (!pg) {
    return reply.code(ERROR).send({'error': 'Do not have database registered'})
  }

  const {body} = request
  const {username} = body

  pg.query('select * from users where username=$1', [username],
    async (err, result) => {
      if (err) {
        return reply.code(ERROR).send({error: 'Cannot select: ' + err})
      }

      if (result.rowCount < 1) {
        return reply.code(USER_DOESNT_EXISTS).send()
      }

      return reply.code(OK).send(result.rows[0])
    }
  )
}

const schema: FastifySchema = {
  tags: ['database'],
  body: {
    type: 'object',
    required: ['username', 'password'],
    properties: {
      username: {type: 'string'},
      password: {type: 'string'}
    },
  },
  response: {
    [OK]: {
      type: 'string',
      description: 'User'
    },
    [USER_DOESNT_EXISTS]: {
      type: 'string',
      description: 'User does not exists'
    },
    [ERROR]: {
      type: 'object',
      descriptions: 'Something went wrong',
      properties: {
        error: { type: 'string' }
      }
    }
  }
}

export const createUser: RouteOptionsWithGeneric<CreateUserRouteGeneric> = {
  method: 'POST',
  url: '/create-user',
  handler,
  schema,
}