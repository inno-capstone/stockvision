import {RouteHandler, RouteOptions} from 'fastify'

const OK = 200
const ERROR = 500

type ListUserHandler = RouteHandler

const handler: ListUserHandler = async (request, reply) => {
  const {pg} = request

  if (!pg) {
    return reply.code(ERROR).send({'error': 'Do not have database registered'})
  }

  try {
    const {rows} = await pg.query('select * from users')
    reply.code(OK).send(rows)
  } catch (e) {
    request.log.error(e)
    reply.code(ERROR).send({error: 'Smt happend: ' + e})
  }
}

export const listUsers: RouteOptions = {
  method: 'POST',
  url: '/list-users',
  handler,
  schema: {
    tags: ['database'],
    response: {
      [OK]: {
        type: 'array',
        description: 'User created',
        items: {
          type: 'object',
          properties: {
            id: {type: 'string'},
            username: {type: 'string'},
            password: {type: 'string'},
            createdAt: {type: 'string'}
          }
        }
      },
      [ERROR]: {
        type: 'object',
        properties: {
          error: {type: 'string'}
        }
      }
    }
  }
}