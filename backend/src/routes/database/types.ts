import {PostgresDb} from "@fastify/postgres"
import {PoolClient} from "pg"

export type PG = PostgresDb

export type PGHandler<T> = (pg: PG) => T

export type OnConnectCallback = (err: Error, client: PoolClient, release: () => void) => void