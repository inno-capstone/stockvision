import postgres from '@fastify/postgres'
import {FastifyPluginCallback} from 'fastify'
import {POSTGRES_CONFIG} from '../../configs'

export const connect: FastifyPluginCallback = (fastify, _opt, done) => {
  fastify.register(postgres, POSTGRES_CONFIG)

  fastify.addHook('preHandler', async (request, reply) => {
    try {
      request.pg = await fastify.pg.connect()
    } catch (e) {
      return reply.code(500).send({error: 'Cannot connect to pg' + e})
    }
  }).addHook('onResponse', (request, _reply) => {
    if (request.pg) {
      request.pg.release()
    }
  })

  done()
}