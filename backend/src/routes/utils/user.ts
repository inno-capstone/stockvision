import {FastifyPluginCallback} from 'fastify'

export const schemas: FastifyPluginCallback = (fastify, _opt, done) => {
  fastify.addSchema({
    $id: 'user',
    type: 'object',
    properties: {
      id: {type: 'number'},
      username: {type: 'string'},
      password: {type: 'string'},
      createdAt: {type: 'string'}
    }
  })
  done()
}