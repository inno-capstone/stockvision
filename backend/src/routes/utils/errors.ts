import {FastifyPluginCallback} from 'fastify'

export const schemas: FastifyPluginCallback = (fastify, _opt, done) => {
  fastify.addSchema({
    $id: 'basicError',
    type: 'object',
    properties: {
      error: {type: 'string'}
    }
  })
  done()
}
