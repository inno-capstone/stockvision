import {RouteOptions} from 'fastify'

const OK = 200
const UNAUTHORIZED = 401

export const signin: RouteOptions = {
  method: 'POST',
  url: '/signin',
  onRequest: async (request, reply) => {
    try {
      console.log(request.headers)
      await request.jwtVerify()
    } catch (err) {
      reply.code(UNAUTHORIZED).send({error: `Token error ${err}`})
    }
  },
  handler: (request, reply) => {
    reply.code(OK).send('ok')
  },
  schema: {
    tags: ['user'],
    headers: {
      Authorization: {type: 'string'}
    },
    response: {
      [OK]: {
        type: 'string',
        description: 'Token',
        example: '123-123'
      },
      [UNAUTHORIZED]: {
        type: 'object',
        description: 'Unauthorized',
        properties: {
          error: {type: 'string'}
        }
      }
    }
  }
}