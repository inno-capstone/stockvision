import {RouteHandler} from 'fastify'
import {validatePassword, validateUsername} from './utils'
import {getUser} from '../../controllers'
import {comparePasswordHash} from '../../utils'
import {RouteOptionsWithGeneric} from '../types'

const OK = 200
const INCORRECT_DATA = 300
const WRONG_USERNAME = 404
const WRONG_PASSWORD = 405
const ERROR = 500

type LoginUserRouteGeneric = {
  Body: {
    username: string
    password: string
  }
}

type LoginUserHandler = RouteHandler<LoginUserRouteGeneric>

const handler: LoginUserHandler = async (request, reply) => {
  const {pg} = request

  if (!pg) {
    return reply.code(ERROR).send({error: 'Do not have database'})
  }

  const {username, password} = request.body

  if (!validateUsername(username) || !validatePassword(password)) {
    return reply.code(INCORRECT_DATA).send()
  }

  try {
    const actualUserResult = await getUser(pg, {username})

    if (actualUserResult.rowCount < 1) {
      return reply.code(WRONG_USERNAME).send()
    }

    const actualUser = actualUserResult.rows[0]

    if (!(await comparePasswordHash(password, actualUser.password))) {
      return reply.code(WRONG_PASSWORD).send()
    }

    const token = await reply.jwtSign({id: actualUser.id})

    return reply.code(OK).send(token)

  } catch (e) {
    return reply.code(500).send({error: 'Something went wrong: ' + e})
  }
}

export const login: RouteOptionsWithGeneric<LoginUserRouteGeneric> = {
  method: 'POST',
  url: '/login',
  handler,
  schema: {
    tags: ['auth'],
    body: {
      username: {type: 'string'},
      password: {type: 'string'}
    },
    response: {
      [OK]: {
        type: 'string',
        description: 'Token',
      },
      [INCORRECT_DATA]: {
        type: 'null',
        description: 'Incorrect data',
      },
      [WRONG_USERNAME]: {
        type: 'null',
        description: 'No such user',
      },
      [WRONG_PASSWORD]: {
        type: 'null',
        description: 'Wrong password',
        properties: {
          error: {type: 'string'}
        }
      },
      [ERROR]: {
        type: 'object',
        description: 'Internal Error',
        properties: {
          error: {type: 'string'}
        }
      },
    }
  }
}