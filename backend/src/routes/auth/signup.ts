import {FastifySchema, RouteHandler} from 'fastify'
import {validatePassword, validateUsername} from './utils'
import {createUser, getUser} from '../../controllers'
import {generatePasswordHash} from '../../utils/password-hashing'
import {RouteOptionsWithGeneric} from '../types'

const OK = 200
const INCORRECT_DATA = 300
const USERNAME_IS_TAKEN = 400
const ERROR = 500

type SignupUserRouteGeneric = {
  Body: {
    username: string
    password: string
  }
}

type SignupUserHandler = RouteHandler<SignupUserRouteGeneric>

const handler: SignupUserHandler = async (request, reply) => {
  const {pg} = request
  if (!pg) {
    return reply.code(ERROR).send({error: 'Do not have connection to database'})
  }

  const {username, password} = request.body
  if (!validateUsername(username)) {
    return reply.code(INCORRECT_DATA).send({error: 'Username is incorrect'})
  }
  if (!validatePassword(password)) {
    return reply.code(INCORRECT_DATA).send({error: 'Password is incorrect'})
  }

  try {
    const anotherUser = await getUser(pg, {username})
    if (anotherUser.rowCount > 0) {
      return reply.code(USERNAME_IS_TAKEN).send()
    }

    const hashedPassword = await generatePasswordHash(password)
    const result = await createUser(pg, {username, hashedPassword})

    const id = result.rows[0].id
    const token = await reply.jwtSign({id})

    return reply.code(OK).send(token)

  } catch (e) {
    reply.code(ERROR).send({error: `Something went wrong: ${e}`})
  }
}

const schema: FastifySchema = {
  tags: ['auth'],
  body: {
    username: {type: 'string'},
    password: {type: 'string'}
  },
  response: {
    [OK]: {
      type: 'string',
      description: 'User created, returning token',
      example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJvb3QiLCJpYXQiOjE2ODgyNTE0Mjh9.MlA6xJD0-Eo_MITrbkLqIfgaZw6O1uu4DQ0uoRHdICU'
    },
    [INCORRECT_DATA]: {
      type: 'object',
      description: 'User data in not valid',
      properties: {
        error: {type: 'string'}
      }
    },
    [USERNAME_IS_TAKEN]: {
      type: 'null',
      description: 'User with that username already exists'
    },
    [ERROR]: {
      type: 'object',
      description: 'Internal Error',
      properties: {
        error: {type: 'string'}
      }
    }
  }
}

export const signup: RouteOptionsWithGeneric<SignupUserRouteGeneric> = {
  method: 'POST',
  url: '/signup',
  handler,
  schema
}