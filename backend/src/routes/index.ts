import jwt from '@fastify/jwt'

import {router as auth} from './auth'
import {router as database} from './database'
import {Router} from './types'
import {JWT_CONFIG} from '../configs'

export const router: Router = (fastify, _opt, done) => {
  fastify.register(jwt, JWT_CONFIG)
  
  // Routes
  fastify
    .register(auth, {prefix: 'auth'})
    .register(database, {prefix: 'database'})
  
  fastify.get('/protected', {
    preHandler: async (request, reply) => {
      fastify.log.info(`Protected request \nHeaders: ${JSON.stringify(request.headers, null, 2)}`)

      try {
        await request.jwtVerify()
        fastify.log.info('JWT is ok')
      } catch (e) {
        return reply.code(401).send({error: `Auth error: ${e}`})
      }
    },
    schema: {
      tags: ['test'],
      headers: {
        'Authorization': {type: 'string'}
      },
      security: [
        {bearerAuth: []}
      ]
    }
  }, async (request, reply) => {
    const data = request.user
    return reply.code(200).send(data)
  })

  done()
}