import {ContextConfigDefault, FastifyBaseLogger, FastifyInstance, FastifyPluginOptions, FastifySchema, FastifyTypeProviderDefault, RawReplyDefaultExpression, RawRequestDefaultExpression, RawServerDefault, RouteGenericInterface, RouteOptions} from 'fastify'

export type ResponseHelper = {
  code: number
  msg?: unknown
}

export type Done = (err?: Error | undefined) => void

export type Router = (server: FastifyInstance, options: FastifyPluginOptions, done: Done) => void

export type RouteOptionsWithGeneric<RouteGeneric extends RouteGenericInterface> = RouteOptions<
  RawServerDefault,
  RawRequestDefaultExpression<RawServerDefault>,
  RawReplyDefaultExpression<RawServerDefault>,
  RouteGeneric,
  ContextConfigDefault,
  FastifySchema,
  FastifyTypeProviderDefault,
  FastifyBaseLogger
>