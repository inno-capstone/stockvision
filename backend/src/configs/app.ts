import path from 'node:path'
import dotenv from 'dotenv'

import * as args from './arguments'

dotenv.config({
  path: path.join(__dirname, '..', '..', '.env')
})

type Environment = 'development' | 'testing' | 'production'

export const isOpenApi: Boolean = Boolean(args.isOpenApi ?? process.env.isOpenApi ?? false)
export const PORT = Number(args.PORT ?? process.env.PORT ?? 3000)
export const ENV: Environment = (args.ENV ?? process.env.ENV ?? 'development') as Environment

export const logger = {
  transport: {
    target: '@fastify/one-line-logger'
  }
}