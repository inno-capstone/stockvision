import {PostgresPluginOptions} from '@fastify/postgres'
import dotenv from 'dotenv'
import path from 'path'

dotenv.config({
  path: path.join(__dirname, '..', '..', '.env.database')
})

const DATABASE_URL = process.env.POSTGRES_USER && 
  process.env.POSTGRES_PASSWORD &&
  process.env.POSTGRES_HOST && 
  process.env.POSTGRES_PORT && 
  process.env.POSTGRES_DB ?
  `postgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}` :
  'postgres://username:password@127.0.0.1:5432/db'

export const POSTGRES_CONFIG: PostgresPluginOptions = {
  connectionString: DATABASE_URL,
  connectionTimeoutMillis: 5_000
}
