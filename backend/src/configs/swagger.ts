import {SwaggerOptions} from "@fastify/swagger"
import {FastifySwaggerUiOptions} from "@fastify/swagger-ui"
import {PORT} from './app'

export const SWAGGER_CONFIG: SwaggerOptions = {
  openapi: {
    info: {
      title: 'Test swagger',
      description: 'testing the fastify swagger api',
      version: '0.1.0'
    },
    servers: [{
      url: `http://127.0.0.1:${PORT}`
    }],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    },
  },
}

export const SWAGGER_UI_CONFIG: FastifySwaggerUiOptions = {
  routePrefix: '/docs',
  uiConfig: {
    docExpansion: 'full',
    deepLinking: false
  },
}