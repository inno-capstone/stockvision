export const PORT = process.argv.find((value) => value.startsWith('port'))?.split('=')?.[1]
export const ENV = process.argv.find((value) => value.startsWith('env'))?.split('=')?.[1]
export const isOpenApi = process.argv.find(value => value.startsWith('--openapi'))