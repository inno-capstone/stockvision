import {FastifyJWTOptions} from '@fastify/jwt'
import dotenv from 'dotenv'

dotenv.config()

export const JWT_CONFIG: FastifyJWTOptions = {
  secret: process.env.jwtSecret ?? 'secret',
}