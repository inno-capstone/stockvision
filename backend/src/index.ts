import Fastify, {FastifyInstance} from 'fastify'

import {router} from './routes'
import * as cfg from './configs'

import swagger from '@fastify/swagger'
import swaggerUI from '@fastify/swagger-ui'
import {isDev} from './constants'

const {PORT, logger, isOpenApi} = cfg

const fastify: FastifyInstance = Fastify({logger})

async function start() {
  if (isOpenApi) {
    await fastify.register(swagger, cfg.SWAGGER_CONFIG)
    await fastify.register(swaggerUI, cfg.SWAGGER_UI_CONFIG)
  }

  await fastify.register(router)

  if (isOpenApi) {
    await fastify.ready()
    fastify.swagger()
  }
  
  fastify.listen({port: PORT}, (err, address) => {
    if (err) {
      fastify.log.error('Cannot listen:', err)
      process.exit(1)
    }

    isDev && console.log({...cfg})

    if (isOpenApi) {
      fastify.log.info(`Docs: ${address}/docs`)
    }
  })
}

start()