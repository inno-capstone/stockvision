import {ENV} from '../configs'

export const isDev = ENV === 'development'