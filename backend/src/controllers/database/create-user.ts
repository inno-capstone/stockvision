import {PoolClient} from 'pg'
import {User} from './types'

interface CreateUserOptions {
  username: string
  hashedPassword: string
}

export function createUser(pg: PoolClient, options: CreateUserOptions) {
  return pg.query<{id: User['id']}>('insert into users(username, password) values ($1, $2) returning id', [options.username, options.hashedPassword])
}