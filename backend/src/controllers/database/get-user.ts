import {PoolClient} from 'pg'
import {User} from './types'

interface GetUserOptions {
  id?: number | string
  username?: string
}

export function getUser(pg: PoolClient, options: GetUserOptions) {
  if (options.id) {
    return pg.query<User>('select * from users where id=$1', [Number(options.id)])
  }
  if (options.username) {
    return pg.query<User>('select * from users where username=$1', [options.username])
  }
  throw new Error('Incorrect controller data in `getUser`')
}