import bcrypt from 'bcrypt'

const SALT_ROUNDS = 5

export async function generatePasswordHash(password: string) {
  return (await bcrypt.hash(password, SALT_ROUNDS))
}

export async function comparePasswordHash(password: string, hash: string) {
  return (await bcrypt.compare(password, hash))
}