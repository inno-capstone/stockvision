import requests
import numpy as np
import pandas as pd



class DataManager(object):

    """
    A class representing a manager for fetching and organising the historical trading data that can be used to build the pytorch dataset.

    Attributes:
        timestep (int): The number of lookback.
        source (string): The source of the historical data.
        url (string): The url from which request will be made to download the data.
    """


    def __init__(self, timestep: int =  24, source: str = "alpha-vantage", **kwargs):
        self.timestep = timestep
        self.source = source
        for key, value in kwargs.items():
            setattr(self, key, value)

        if self.source == "alpha-vantage":
            self.url = f'https://www.alphavantage.co/query?function=CRYPTO_INTRADAY&symbol={self.from_symbol}&market={self.to_symbol}&interval={self.interval}&outputsize={self.outputsize}&datatype={self.datatype}&apikey={self.api_key}'


    def fetch_data(self) -> pd.DataFrame:
        """
        Gets the data via a HTTP request.

        Returns:
            (DataFrame): This contains time, open, hign, low, close and volume as the features.
        """
        fetch = False
        while not fetch:
          try:
            response = requests.get(self.url)
            data = response.json()
            time_series = data['Time Series Crypto (60min)']
            fetch = True
          except Exception as e:
            print("Alpha Vantage currently unavailable")
            print("Trying again...")
            fetch = False
          finally:
            continue

        data_df = pd.DataFrame.from_dict(time_series, orient='index')
        data_df.reset_index(inplace=True)
        data_df.columns = ['time', 'open', 'high', 'low', 'close', 'volume']
        data_df['time'] = pd.to_datetime(data_df['time'])
        data_df.sort_values("time", ascending=True, inplace=True)
        data_df.reset_index(drop=True, inplace=True)
        data_df['open'] = pd.to_numeric(data_df['open'])
        data_df['close'] = pd.to_numeric(data_df['close'])
        start_date = '2021-01-01'
        data_df = data_df[data_df['time'] >= start_date]
        data_df["time"] = data_df["time"].apply(lambda x : int(x.timestamp()))
        return data_df


    def get_current_price(self) -> float:
        """
        Gets the current price of the currency pair.

        Returns:
            (float): This is the current price of the pair.
        """

        url = f"https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency={self.from_symbol}&to_currency={self.to_symbol}&apikey={self.api_key}"
        r = requests.get(url)
        data = dict(r.json())
        return float(data['Realtime Currency Exchange Rate']['5. Exchange Rate'])


    def generate_signals(self, data: pd.DataFrame, drop_last = True) -> pd.DataFrame:
        """
        Generates the signals that will be used to train the model.

        Returns:
            (DataFrame): This is the dataframe that has the time, open and the signal that will be used to train the model.
        """
        choosen_columns = ["time", "open", "close"]
        data = data[choosen_columns]
        total = len(data)
        signals = []
        for idx in range(total):
            signal = 1 if data["open"].iloc[idx] < data["close"].iloc[idx] else 0
            signals.append(signal)

        data["signal"] = signals
        if drop_last:
            data = data.iloc[:-1, :]
        return data[["time", "open", "signal"]]


    def get_test_data(self, scaler) -> pd.DataFrame:
        """
        Get the data we will use for getting the predictions of the university.

        Returns:
            (DataFrame): This contains the last timesteps open prices that will be used during inference to get the next.
        """
        data = self.fetch_data()
        data = self.generate_signals(data, drop_last= False)
        data = data.iloc[(-1 * self.timestep):, :]
        #data = data.iloc[-1:, :]
        #print(data)
        data = scaler.transform(data[["time", "open"]])
        return data.reshape(1, -1, 2)


