import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from loss import AdamWithWarmUp
from torch.optim import Adam
from data import DataManager
from dataset import CryptoDataset
from model import CryptoModel, CryptoLSTM
torch.autograd.set_detect_anomaly(True)


device = "cuda" if torch.cuda.is_available() else "cpu"
manager = DataManager(from_symbol = "BTC", to_symbol = "USD", api_key = "QL9P1ZJOYJKVSKOZ", interval = '60min',outputsize = 'full' ,datatype = 'json') 
dataset = CryptoDataset(manager= manager)
train_loader = DataLoader(dataset= dataset, batch_size=6)
epochs = 250
model = CryptoModel(manager=manager, d_model=512, n_head=8, layers=5)
model = CryptoLSTM(manager = manager, num_layers=2, dropout = 0.5).to(device)
optimizer = Adam(lr= 0.001, params=model.parameters())
adamWithWarmUp = AdamWithWarmUp(warmup_steps=3, optimizer=optimizer, d_model=512)
criterion = nn.BCELoss(reduction="sum")


def run_training():
    for epoch in range(epochs):
        epoch_loss = 0
        model.train()
        for batch_idx , (data, target) in enumerate(train_loader):
            states = model.init_hidden_states(batch_size=data.shape[0])
            out = model(data.float(), states)
            out = out.view(-1)
            target = target.view(-1).float()
            batch_loss = criterion(out, target)
            epoch_loss += batch_loss.item()
            optimizer.zero_grad()
            batch_loss.backward()
            optimizer.step()
            #adamWithWarmUp.step()
            with torch.no_grad():
                print(list(map(int, (out > 0.5).data)))
        print(f"Epoch {epoch + 1}/ {epochs} Loss : {epoch_loss/len(dataset)}")
    model.eval()
    return model
    