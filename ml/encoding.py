import torch
from torch import nn
import numpy as np
import math

def t2v(tau, f, out_features, w, b, w0, b0, arg=None):
    if arg:
        v1 = f(torch.matmul(tau, w) + b, arg)
    else:
        v1 = f(torch.matmul(tau, w) + b)
    v2 = torch.matmul(tau, w0) + b0
    return torch.cat([v1, v2], -1)

class SineActivation(nn.Module):
    def __init__(self, in_features, out_features):
        super(SineActivation, self).__init__()
        self.out_features = out_features
        self.w0 = nn.parameter.Parameter(torch.randn(in_features, 1))
        self.b0 = nn.parameter.Parameter(torch.randn(1))
        self.w = nn.parameter.Parameter(torch.randn(in_features, out_features-1))
        self.b = nn.parameter.Parameter(torch.randn(out_features-1))
        self.f = torch.sin

        assert self.w0.requires_grad == True, "Parameter not trainable"
        assert self.b0.requires_grad == True, "Parameter not trainable"
        assert self.w.requires_grad == True, "Parameter not trainable"
        assert self.b.requires_grad == True, "Parameter not trainable"



    def forward(self, tau):
        tau.unsqueeze_(2)
        return t2v(tau, self.f, self.out_features, self.w, self.b, self.w0, self.b0)
    
class PriceEmbedding(nn.Module):
    def __init__(self, in_features = 1, out_features = 512):
        super(PriceEmbedding, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.l1 = nn.Linear(in_features=in_features, out_features=out_features)

    def forward(self, x):
        x.unsqueeze_(2)
        return self.l1(x)
    
class PositionalEncoding(nn.Module):
    def __init__(self, d_model = 512, timestep = 6):
        super(PositionalEncoding, self).__init__()
        self.d_model = d_model
        self.timestep = timestep
        self.pe = self.create_positional_emebddings(timestep=self.timestep, d_model=self.d_model)


    def create_positional_emebddings(self, timestep, d_model):
        pe = torch.zeros(timestep, d_model)
        for pos in range(timestep):
            for i in range(0, d_model, 2):
                pe[pos, i] = math.sin(pos / (10000 **((2 * i)/ d_model)))
                pe[pos, i + 1] = math.cos(pos / (10000 **((2 * (i + 1))/ d_model)))
        pe = pe.unsqueeze(0)
        return pe
    

    def forward(self, embedding):
        assert embedding.shape[-1] == self.d_model
        embedding = embedding * math.sqrt(self.d_model)
        embedding += self.pe
        return embedding


