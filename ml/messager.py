import pika
import json
import datetime
import os
import requests

currencies = [("BTC", "USD"), ] # currencies is a list of supported currencies we have.
frequencies = [60] #This is the list of supported frequencies we have at he backend.
frequencies_to_alpha_mapper = {60: "60min"} #This is used to map the frequency to a valid string to be used the broker.
def getRoutingKeys():
    '''This returns a list of routing keys that will be used to send signals through the KAFKA queue to the backend.'''
    
    keys = []
    running_key = ""
    for currency in currencies:
        running_key += (currency[0] + currency[1])
        running_key += "-"
        for freq in frequencies:
            running_key += str(freq)
            keys.append(running_key)
            running_key = running_key.split("-")[0]
        running_key = ""
    return keys

def send_to_endpoint(key = "BTCUSD-60", signal= None, prediction_time  = None, certainty:float= None, importance: dict= None):
    if signal == None:
        return
    signal = "BUY" if signal else "SELL"
    print(f"Sending to endpoint the signal is {signal} for the time {prediction_time}")
    data = {'time': str(prediction_time.strftime('%Y-%m-%d %H:%M:%S')), 'currency': "BTCUSD", "signal": signal, "certainty": certainty, "key": 'BTCUSD-60', "importance" : importance}
    url = "http://backend-service:80/signal"
    sent = False
    while not sent:
        try:
            response = requests.post(url, json=data)
            if response.status_code == 200:
                sent = True
                continue
        except Exception as e:
            sent = False
        
        finally:
            continue
        


def send_to_queue(key = "BTCUSD-60", signal= None, prediction_time  = None, certainty:float= None, importance:dict = None):
    if signal == None:
        return
    signal = "BUY" if signal else "SELL"
    print(f"Sending to queue the signal is {signal} for the time {prediction_time}")
    conn = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = conn.channel()

    # {"currency": "EUR/USD", "time": "14:00:000", "signal": "BUY", "certainty": 0.89}
    channel.exchange_declare(exchange='amq.topic', exchange_type='topic', durable=True)
    data = {'time': str(prediction_time.strftime('%Y-%m-%d %H:%M:%S')), 'currency': "BTCUSD", "signal": signal, "certainty": certainty, "importance" : importance}
    message = json.dumps(data)

    routing_key = 'BTCUSD-60'
    channel.basic_publish(exchange='amq.topic', routing_key=routing_key, body=message)
    conn.close()
