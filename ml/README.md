This is the code that will do the training of the model at every o'clock and send via a rabbit mq to the backend service. 

The exchange type in the queue will be a direct exchange i.e we will use the currency_pair "EUR/USD" as the binding key.

This queue and binding will be created at the consumer end, whist the producer (ml service) will send the message to the appropraite queue

The format of the message is a json example {"currency": "EUR/USD", "time": "14:00:000", "signal": "BUY", "certainty": 0.89}

The signal can either be "BUY" or "SELL" and the certainty is a percentage of how strong the buy or sell is.



