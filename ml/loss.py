from torch.optim import Optimizer

class AdamWithWarmUp:
    def __init__(self, warmup_steps:int, optimizer: Optimizer, d_model:int = 512 ):
        self.current_step = 0
        self.lr = 0
        self.warmup_step = warmup_steps
        self.optimizer = optimizer
        self.d_model = d_model

    def get_lr(self):
        return self.d_model ** (-0.5) * min(self.current_step ** (-0.5), self.current_step * self.warmup_step ** (-1.5))

    def step(self):
        self.current_step += 1
        lr = self.get_lr()
        for param_groups in self.optimizer.param_groups:
            param_groups['lr'] = lr
        self.optimizer.step()
        
        