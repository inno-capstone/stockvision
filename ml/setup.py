import datetime
import time
from train import run_training, dataset
import torch.nn.functional as F
from messager import send_to_queue, send_to_endpoint
import torch

device = "cuda" if torch.cuda.is_available() else "cpu"



def wait_until_next_hour():
    now = datetime.datetime.now()
    next_hour = now.replace(minute=0, second=0, microsecond=0) + datetime.timedelta(hours=1)
    remaining_time = (next_hour - now).total_seconds() / 60  # Convert remaining time to minutes
    if remaining_time > 0:
        print(f"It's not yet the next hour. Waiting for {remaining_time:.2f} minutes...")
        while remaining_time > 0:
            time.sleep(60)  # Sleep for 1 minute
            remaining_time -= 1
            print(f"{remaining_time:.2f} minutes remaining...")
            

def get_importance(model):
    with torch.no_grad():
        layer = model.fc5.weight.data
        layer = layer.view(model.manager.timestep)
        layer = (F.softmax(layer.clone().cpu()) * 100).tolist()
        now = datetime.datetime.now()
        now = now.replace(minute=0, second=0, microsecond=0)
        times = sorted([str((now - datetime.timedelta(hours=i)).strftime('%Y-%m-%d %H:%M:%S')) for i in range(model.manager.timestep)])
        return dict(zip(times, layer))
        
        


def process_currencies():
    model = run_training()
    importance = get_importance(model)
    scaler = dataset.scaler
    test_data = dataset.manager.get_test_data(scaler=  scaler)
    test_data = torch.from_numpy(test_data).float().to(device)
    states =  model.init_hidden_states(batch_size=1)
    predictions = model(test_data, states).cpu().view(-1)
    val = (predictions >= 0.5)[-1].item()
    now = datetime.datetime.now()
    now = now.replace(minute=0, second=0, microsecond=0)
    certainty = predictions[-1].item() if val else 1 - predictions[-1].item()
    send_to_endpoint(key = "BTCUSD-60", signal = val,prediction_time=now, certainty = certainty, importance = importance)

    
def currency_processing_job():
    while True:
        wait_until_next_hour()
        process_currencies()
