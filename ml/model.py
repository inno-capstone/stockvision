import torch
import torch.nn as nn
import torch.nn.functional as F
from encoding import PriceEmbedding,PositionalEncoding, SineActivation
from data import DataManager

class MultiHeadAttention(nn.Module):
    def __init__(self,n_heads, d_model):
        super(MultiHeadAttention, self).__init__()
        assert d_model % n_heads == 0, f"Cannot split the model into {n_heads} heads"
        self.n_heads  = n_heads
        self.d_model = d_model
        self.dk = self.d_model // self.n_heads

        self.query  = nn.Linear(self.d_model, self.d_model)
        self.key = nn.Linear(self.d_model, self.d_model)
        self.value = nn.Linear(self.d_model, self.d_model)
        self.concat = nn.Linear(self.d_model, self.d_model)


    def forward(self, query, key, value):
        query = self.query(query)
        key = self.key(key)
        value = self.value(value)

        query = query.view(query.shape[0], -1, self.n_heads, self.dk).permute(0,2,1,3)
        value = value.view(value.shape[0], -1, self.n_heads, self.dk).permute(0,2,1,3)
        key = key.view(key.shape[0], -1, self.n_heads, self.dk).permute(0,2,1,3)
        attention_weight  = F.softmax(torch.matmul(query, key.permute(0, 1, 3, 2))/ self.dk, dim=-1)
        context_vector = torch.matmul(attention_weight, value)

        context_vector = context_vector.permute(0, 2, 1, 3).reshape(context_vector.shape[0], -1, self.dk * self.n_heads)
        interacted = self.concat(context_vector)
        return interacted
    

class FeedForward(nn.Module):
    def __init__(self, d_model, hidden_size = 2048):
        super(FeedForward, self).__init__()
        self.d_model = d_model
        self.hidden_size = hidden_size
        self.l1 = nn.Linear(self.d_model, self.hidden_size)
        self.l2 = nn.Linear(self.hidden_size, self.d_model)

    def forward(self, x):
        x = F.relu(self.l1(x))
        return self.l2(x)



class Encoder(nn.Module):
    def __init__(self,d_model = 512, heads = 8):
        super(Encoder, self).__init__()
        self.self_multi_head_attention = MultiHeadAttention(n_heads=heads, d_model=d_model)
        self.feed_forward = FeedForward(d_model=d_model)
        self.layer_norm = nn.LayerNorm(d_model)

    def forward(self, embedding):
        interaced = self.self_multi_head_attention(embedding, embedding, embedding)
        interaced += embedding
        normalized = self.layer_norm(interaced)

        interaced = self.feed_forward(normalized)
        interaced += normalized

        encoded = self.layer_norm(interaced)

        return encoded
    

class TransformerWODecoder(nn.Module):
    def __init__(self,feature: str, manager: DataManager, d_model:int = 512, n_head:int = 8, layers:int = 5):
        super(TransformerWODecoder, self).__init__()
        self.manager = manager
        self.d_model = d_model
        self.n_head = n_head
        self.layers = layers
        self.encoder_blocks = nn.Sequential(*[Encoder(d_model=self.d_model, heads=self.n_head) for _ in range(self.layers)])
        self.embed = self.get_embed_layer(feature=feature)

    def get_embed_layer(self, feature):
        if feature == "datetime":
            return  nn.Sequential(
                SineActivation(1, out_features=self.d_model),
                PositionalEncoding(d_model=self.d_model,timestep=self.manager.timestep)
            )
        elif feature == "price":
            return nn.Sequential(
                PriceEmbedding(in_features=1, out_features=self.d_model),
                PositionalEncoding(d_model=self.d_model,timestep=self.manager.timestep)
            )
            
        else:
            raise Exception("unsupported transformer type")


    def forward(self, x):
        embedding = self.embed(x)
        for layer in self.encoder_blocks:
            embedding = layer(embedding)
        return embedding
         

class CryptoModel(nn.Module):
    def __init__(self,manager: DataManager, d_model:int = 512, n_head:int = 8, layers:int = 5):
        super(CryptoModel, self).__init__()
        self.manager = manager
        self.price_model = TransformerWODecoder(feature="price", manager=manager, d_model=d_model, n_head=n_head, layers=layers)
        self.datetime_model = TransformerWODecoder(feature="datetime", manager=manager, d_model= d_model, n_head=n_head, layers=layers)
        self.fc1 = nn.Linear(d_model * 2, d_model)
        self.fc2 = nn.Linear(d_model, 1)

    def forward(self, x):
        time_data = x[:, :, 0].clone()
        price_data = x[:, :, 1].clone()

        time_encoded = self.datetime_model(time_data)
        price_encoded = self.price_model(price_data)

        encoding = torch.cat([time_encoded, price_encoded], dim=2)
        out = F.relu(self.fc1(encoding))
        return F.sigmoid(self.fc2(out))
    

class CryptoLSTM(nn.Module):
    def __init__(self, manager:DataManager, input_size = 2, hidden_size = 512, output_size = 1, num_layers = 2, dropout = 0.5):
        super(CryptoLSTM, self).__init__()
        self.num_layers = num_layers
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.manager = manager
        self.output_size = output_size
        self.lstm = nn.LSTM(input_size = input_size, hidden_size = hidden_size, num_layers = num_layers, batch_first = True, dropout = dropout)
        self.fc1 = nn.Linear(self.hidden_size, self.hidden_size)
        self.fc2 = nn.Linear(self.hidden_size, self.hidden_size)
        self.fc3 = nn.Linear(self.hidden_size, self.hidden_size)
        self.fc4 = nn.Linear(self.hidden_size, manager.timestep)
        self.fc5 = nn.Linear(manager.timestep, self.output_size)
        self.bnorm = nn.BatchNorm1d(manager.timestep)

    def init_hidden_states(self, batch_size):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        h0 = torch.zeros(self.num_layers, batch_size, self.hidden_size).to(device)
        c0 = torch.zeros(self.num_layers, batch_size, self.hidden_size).to(device)
        return (h0, c0)

    def forward(self, data, states):
        out, _ = self.lstm(data, states)
        out = F.mish(self.fc1(out))
        out = F.mish(self.fc2(out))
        out = F.mish(self.fc3(out))
        out = F.mish(self.fc4(out))
        return F.sigmoid(self.fc5(out))


