import torch
import numpy as np
from torch.utils.data import Dataset, DataLoader
from data import DataManager
from sklearn.preprocessing import MinMaxScaler, StandardScaler


device = "cuda" if torch.cuda.is_available() else "cpu"


class CryptoDataset(Dataset):

    def __init__(self, manager: DataManager):
        self.manager = manager
        data = manager.fetch_data()
        self.data = manager.generate_signals(data)
        self.scaler = StandardScaler()
        self.build_sequence()

    def build_sequence(self) -> np.ndarray:
        timestep = self.manager.timestep
        self.scaler.fit(self.data.iloc[:, :-1])
        x = self.scaler.transform(self.data.iloc[:, :-1])
        #x = self.data.iloc[:, :-1].values
        x = x.reshape(-1, 2)
        y = self.data.iloc[:, -1]

        features = []
        target = []
        for i in range(len(self.data), -1, -1 * timestep):
            step = []
            step_y = []
            for j in range(i- timestep, i):
                step.append(list(x[j, :]))
                step_y.append(int(y.iloc[j]))
            if len(step) == timestep:
                features.insert(0,step)
                target.insert(0,step_y)
        self.features = np.array(features)
        self.target = np.array(target)


    def __getitem__(self, index) -> tuple:
        x = self.features[index, :]
        y = self.target[index]

        x = torch.from_numpy(x).reshape(self.manager.timestep, 2)
        y = torch.from_numpy(y)
        return x, y

    def __len__(self):
        return len(self.features)
    
